﻿using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eDok.IdentityServer
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetAllApiresources()
        {
            return new List<ApiResource>
            {
                new ApiResource("eDokApi", "Customer api for eDokApi")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId= "client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {"eDokApi"}
                }
            };
        }
    }
}
