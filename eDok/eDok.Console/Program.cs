﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace eDok.Console
{
    class Program
    {
        static void Main(string[] args) => MainAsync().GetAwaiter().GetResult();

        private static async Task MainAsync()
        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("http://localhost:5103/");

            if (disco.IsError) throw new Exception(disco.Error);

            var tokenEndPoint = disco.TokenEndpoint;
            var response = await client.RequestTokenAsync(new TokenRequest
            {
                Address = tokenEndPoint,
                GrantType = "client_credentials",
                ClientId = "client",
                ClientSecret = "secret",
                Parameters =
                {
                    {"scope" , "eDokApi" }
                }
            });

            var accessToken = response.AccessToken;
            System.Console.WriteLine(accessToken);
            System.Console.WriteLine("\n");

            var userInfo = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = disco.UserInfoEndpoint,
                Token = accessToken
            });

            var claims = userInfo.Claims;
            System.Console.WriteLine(string.Join(",",claims));
            System.Console.WriteLine("\n");

            client.SetBearerToken(accessToken);

            var customerInfo = new StringContent(
                JsonConvert.SerializeObject(
                new { Id = 0, FirstName = "Umair2", LastName = "Saeed" }
                ), Encoding.UTF8, "application/json");

            var createCustomerResponse = await client.PostAsync("http://localhost:5105/api/customers", customerInfo);

            if (!createCustomerResponse.IsSuccessStatusCode)
            {
                System.Console.WriteLine(createCustomerResponse.StatusCode);
            }

            var getCustomerInfo = await client.GetAsync("http://localhost:5105/api/customers");
            if(!getCustomerInfo.IsSuccessStatusCode)
            {
                System.Console.WriteLine(getCustomerInfo.StatusCode);
            }

            var content = await getCustomerInfo.Content.ReadAsStringAsync();
            var customer = JArray.Parse(content);
            System.Console.WriteLine(customer);

            System.Console.ReadKey();
            
        }
    }
}   
